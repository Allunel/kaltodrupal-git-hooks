# README #

##Helper hooks for **KaltoDrupal** project.##

### What is this repository for? ###
Only feature/support/hotfix/release from git flow is now supported. It's generating automaticly issue number and branch type from git flow branch. E.g.:

`git flow feature start 1234-user-reports`

..from now we have branch `feature/1234-user-reporst` and after each commit into this branch you will get 

`#issuenumber (branchtype): original commit message` in above case if we commit as follow:

`git commit -m 'Adding basic functionality'` the we the final commit message will be:

`#1234 (feature): Adding basic functionality`

### How do I get set up? ###
1. Go to download and get latest **tag** version.
1. Unpack in your `[kaltorepo]/.git/hooks`
1. Start using